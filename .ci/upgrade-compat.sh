#!/bin/sh -e
# Copyright 2023 Dylan Van Assche, Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
# Description: test upgrade.sh in pmb chroot (use env vars to set CHANNEL_OLD etc)
# Options: native slow
# https://postmarketos.org/pmb-ci

if [ "$(id -u)" = 0 ]; then
	set -x
	wget "https://gitlab.com/postmarketOS/ci-common/-/raw/master/install_pmbootstrap.sh"
	sh ./install_pmbootstrap.sh
	exec su "${TESTUSER:-pmos}" -c "sh -e $0"
fi

if [ -z "$(command -v pmbootstrap 2>/dev/null)" ]; then
	echo "ERROR: install pmbootstrap first, consider using 'pmbootstrap ci'"
	echo "       https://postmarketos.org/pmbootstrap"
	exit 1
fi

set -x
DEVICE="${DEVICE:-qemu-amd64}"
UI="${UI:-none}"
CHANNEL_OLD="${CHANNEL_OLD:-v22.12}"
CHANNEL_NEW="${CHANNEL_NEW:-edge}"

PMAPORTS="$(pmbootstrap -q config aports)"
WORK="$(pmbootstrap -q config work)"

# Switch branch and release channel
git -C "$PMAPORTS" checkout "$CHANNEL_OLD"

# Configure device & UI
pmbootstrap config device "$DEVICE"
pmbootstrap config ui "$UI"

# Quirk for apk solver bug
# https://gitlab.alpinelinux.org/alpine/apk-tools/-/issues/10843
if [ "$CHANNEL_OLD,$UI,$DEVICE" = "v21.12,sxmo-de-sway,qemu-amd64" ]; then
	pmbootstrap --details-to-stdout chroot -r apk add device-qemu-amd64-sway
fi

# Zap any existing chroots in case the user runs this via "pmbootstrap ci"
pmbootstrap -y zap -p

# Create rootfs
pmbootstrap --details-to-stdout install --no-image --password test

# Execute upgrade in rootfs
cp upgrade.sh "$WORK"/chroot_rootfs_"$DEVICE"/tmp/upgrade.sh
yes | pmbootstrap -q chroot -r -- /tmp/upgrade.sh "$CHANNEL_NEW"
