#!/bin/sh -e
# Copyright 2023 Oliver Smith
# Copyright 2022 Dylan Van Assche
# SPDX-License-Identifier: GPL-3.0-or-later
# Description: lint all shell scripts
# https://postmarketos.org/pmb-ci

DIR="$(cd "$(dirname "$0")/.." && pwd -P)"

if [ "$(id -u)" = 0 ]; then
	set -x
	apk -q add shellcheck
	exec su "${TESTUSER:-build}" -c "sh -e $0"
fi

if [ -z "$(command -v shellcheck 2>/dev/null)" ]; then
	echo "ERROR: install shellcheck or use 'pmbootstrap ci'"
	exit 1
fi

# Shell: shellcheck
sh_files="
	$(find . -path '*.sh')
"

for file in $sh_files; do
	echo "Test with shellcheck: $file"
	cd "$DIR/$(dirname "$file")"
	shellcheck -e SC1008,SC3043 -x "$(basename "$file")"
done
