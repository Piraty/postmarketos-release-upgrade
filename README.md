# postmarketos-release-upgrade

Release upgrade tool, to get from one postmarketOS release to another one. See
https://postmarketos.org/upgrade for details.
